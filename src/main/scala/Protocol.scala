import java.util.UUID

import Protocol.ProductProtocol.{CreateProduct, ProductCreated}

object Protocol extends App {

  trait DomainCommand

  trait DomainEvent

  trait ProtocolDef {

    trait ProtocolCommand extends DomainCommand

    trait ProtocolEvent extends DomainEvent

  }

  trait Aggregate {
    type Protocol <: ProtocolDef
  }

  object ProductProtocol extends ProtocolDef {

    case class CreateProduct(name: String, price: Double) extends DomainCommand

    case class ProductCreated(name: String, price: Double, id: String) extends DomainEvent

  }

  case class Product(name: String, price: Double) extends Aggregate {
    type Protocol = ProductProtocol.type
  }

  trait Behaviour[A <: Aggregate] {
    type AggregateType = A
    type Command = AggregateType#Protocol#ProtocolCommand
    type Event = AggregateType#Protocol#ProtocolEvent

    def validate(cmd: Command)

    def applyEvent(evt: Event): AggregateType
  }

  val behaviour = new Behaviour[Product] {
    def validate(cmd: Command) = {
      cmd match {
        case c: CreateProduct => ProductCreated(c.name, c.price, UUID.randomUUID().toString)
      }
    }

    def applyEvent(evt: Event): Product = {
      evt match {
        case e: ProductCreated => Product(e.name, e.price)
      }
    }
  }

  //Problem this line compiles as "validate" accepts time DomainCommand and this is too broad
    def call(): DomainEvent = behaviour.validate(new Command {})

  //  call()
}